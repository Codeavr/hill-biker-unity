using System.Collections.Generic;
using UnityEngine;

namespace MeshGeneration
{
    public struct ChunkMeshGenerationData
    {
        public readonly IReadOnlyList<Vector2> Points;
        public readonly float Height;
        public readonly float Width;

        public ChunkMeshGenerationData(IReadOnlyList<Vector2> points, float height, float width)
        {
            Points = points;
            Height = height;
            Width = width;
        }
    }
}