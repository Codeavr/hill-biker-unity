using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace MeshGeneration
{
    public static class ChunkMeshGenerator
    {
        public static void Generate(ChunkMeshGenerationData chunkMeshData, Mesh mesh)
        {
            List<Vector3> vertices = CreateVertices(chunkMeshData.Points, chunkMeshData.Height);
            List<int> triangles = CreateTriangleIndices(vertices);
            Vector2[] uvs = CreateUVs(vertices, chunkMeshData.Width);

            mesh.Clear();
            mesh.SetVertices(vertices);
            mesh.SetTriangles(triangles, 0);
            mesh.uv = uvs;
        }

        public static Mesh Generate(ChunkMeshGenerationData chunkMeshData)
        {
            Mesh mesh = new Mesh();
            Generate(chunkMeshData, mesh);
            return mesh;
        }

        private static Vector2[] CreateUVs(IReadOnlyList<Vector3> vertices, float width)
        {
            var uvs = new Vector2[vertices.Count];

            for (int i = 0; i < uvs.Length; i++)
            {
                uvs[i] = new Vector2(vertices[i].x / width, vertices[i].y / width);
            }

            return uvs;
        }

        private static List<int> CreateTriangleIndices(IReadOnlyCollection<Vector3> vertices)
        {
            var triangles = new List<int>();

            for (var i = 0; i < vertices.Count - 2; i++)
            {
                if (i % 2 != 0)
                {
                    triangles.Add(i);
                    triangles.Add(i + 1);
                }
                else
                {
                    triangles.Add(i + 1);
                    triangles.Add(i);
                }

                triangles.Add(i + 2);
            }

            return triangles;
        }

        private static List<Vector3> CreateVertices(IEnumerable<Vector2> points, float height)
        {
            var vertices = new List<Vector3>();

            foreach (Vector2 point in points)
            {
                vertices.Add(point);
                vertices.Add(point - Vector2.up * height);
            }

            return vertices;
        }
    }
}