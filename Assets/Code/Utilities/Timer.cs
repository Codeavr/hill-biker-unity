using System;
using UnityEngine;

public class Timer
{
    public enum TimerMode
    {
        Single,
        Repeat
    }

    public float StartTime { get; private set; }

    public float Duration { get; set; }

    public float EndTime => StartTime + Duration;

    public bool IsActive { get; set; }

    public TimerMode Mode { get; set; }

    public event Action EndTimeEvent;

    public Timer(float duration, TimerMode mode = TimerMode.Single, Action endTimeEventCallback = null)
    {
        StartTime = Time.unscaledTime;
        Duration = duration;
        Mode = mode;

        if (endTimeEventCallback != null)
        {
            EndTimeEvent += endTimeEventCallback;
        }
    }

    public void Tick()
    {
        if (!IsActive)
        {
            return;
        }

        if (Time.time >= EndTime)
        {
            EndTimeEvent?.Invoke();

            if (Mode == TimerMode.Single)
            {
                IsActive = false;
            }
            else
            {
                float error = Time.time - EndTime;
                StartTime = Time.time - error;
            }
        }
    }

    public void Start()
    {
        IsActive = true;
        StartTime = Time.time;
    }

    public void Reset() => Start();

    public void Stop()
    {
        IsActive = false;
    }
}