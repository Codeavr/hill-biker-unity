using System;
using System.Globalization;
using Game;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UIDistanceToText : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text text;

        [SerializeField]
        private TravelStatistics travelStatistics;

        private int _lastInteger = 0;

        private void Awake()
        {
            text.SetText(_lastInteger.ToString());
        }

        private void Update()
        {
            int integer = Mathf.FloorToInt(travelStatistics.Distance);

            if (integer != _lastInteger)
            {
                _lastInteger = integer;
                text.SetText(integer.ToString());
            }
        }
    }
}