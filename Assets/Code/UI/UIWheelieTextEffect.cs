using System;
using Game;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UIWheelieTextEffect : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text text;

        [SerializeField]
        private WheelieDetector wheelie;

        [SerializeField]
        private Color regularColor = new Color(1, 1, 1, 0);

        [SerializeField]
        private Color wheelieColor = new Color(1, 1, 1, 1);

        private void Awake()
        {
            text.color = regularColor;
        }

        private void OnEnable()
        {
            wheelie.WheelieStartedEvent += OnWheelieStarted;
            wheelie.WheelieEndedEvent += OnWheelieEnded;
        }

        private void OnDisable()
        {
            wheelie.WheelieStartedEvent -= OnWheelieStarted;
            wheelie.WheelieEndedEvent -= OnWheelieEnded;
        }

        private void OnWheelieEnded()
        {
            text.color = regularColor;
        }

        private void OnWheelieStarted()
        {
            text.color = wheelieColor;
        }
    }
}