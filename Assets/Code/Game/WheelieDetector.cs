using System;
using Bike;
using UnityEngine;

namespace Game
{
    public class WheelieDetector : MonoBehaviour
    {
        public bool Detected { get; private set; }

        public event Action WheelieStartedEvent;
        public event Action WheelieEndedEvent;

        [SerializeField]
        private BikeObject bike;

        [SerializeField]
        private float rearThreshold = 0.1f;

        [SerializeField]
        private float frontThreshold = 0.1f;

        private float _frontWheelInAirTime;

        private float _rearWheelInAirTime;

        private void Update()
        {
            UpdateTimings();

            if (_rearWheelInAirTime < rearThreshold &&
                _frontWheelInAirTime > frontThreshold)
            {
                if (!Detected)
                {
                    Detected = true;
                    WheelieStartedEvent?.Invoke();
                }
            }
            else
            {
                if (Detected)
                {
                    Detected = false;
                    WheelieEndedEvent?.Invoke();
                }
            }
        }

        private void UpdateTimings()
        {
            if (!bike.RearWheel.IsOnGround())
            {
                _rearWheelInAirTime += Time.deltaTime;
            }
            else
            {
                _rearWheelInAirTime = 0;
            }

            if (!bike.FrontWheel.IsOnGround())
            {
                _frontWheelInAirTime += Time.deltaTime;
            }
            else
            {
                _frontWheelInAirTime = 0;
            }
        }
    }
}