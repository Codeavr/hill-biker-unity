using System;
using UnityEngine;

namespace Game
{
    public class TravelStatistics : MonoBehaviour
    {
        public float Distance { get; private set; }

        [SerializeField]
        private Transform trackedObject;

        [SerializeField]
        private Transform startingPoint;

        private void Update()
        {
            if (!trackedObject)
            {
                return;
            }
            
            Distance = Mathf.Max(trackedObject.position.x - startingPoint.position.x, Distance);
        }

        public void SetTrackedObject(Transform tracked)
        {
            trackedObject = tracked;
        }

        public void Reset()
        {
            Distance = 0;
        }
    }
}