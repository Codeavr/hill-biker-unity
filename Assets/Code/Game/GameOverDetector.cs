﻿using System;
using Bike;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameOverDetector : MonoBehaviour
    {
        [SerializeField]
        private Body bikeBody;

        private void Update()
        {
            if (bikeBody.IsTouchingGround())
            {
                ReloadLevel();
            }
        }

        private void ReloadLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}