using System;
using Bike.Input;
using UnityEngine;

namespace Bike
{
    public class BikeControl : MonoBehaviour
    {
        [SerializeField]
        private BikeObject bikeObject;

        private IVehicleInput _input;

        private void Start()
        {
            _input = Application.platform == RuntimePlatform.Android ? (IVehicleInput) new TouchVehicleInput() : new KeyboardVehicleInput();
        }

        private void Update()
        {
            _input.Tick();
        }

        private void FixedUpdate()
        {
            if (_input.Accelerate)
            {
                bikeObject.Accelerate(Time.fixedDeltaTime);
            }

            if (_input.Brake)
            {
                bikeObject.Brake(Time.fixedDeltaTime);
            }

            if (_input.Jump)
            {
                bikeObject.Jump(Time.fixedDeltaTime);
            }
            
            _input.Reset();
        }
    }
}