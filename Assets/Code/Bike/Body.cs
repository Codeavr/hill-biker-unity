using System;
using UnityEngine;

namespace Bike
{
    public class Body : MonoBehaviour
    {
        [SerializeField]
        private Collider2D collider;

        [SerializeField]
        private ContactFilter2D touchingGroundFilter;

        public bool IsTouchingGround()
        {
            return collider.IsTouching(touchingGroundFilter);
        }
    }
}