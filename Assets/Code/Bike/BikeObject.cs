﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Bike
{
    public class BikeObject : MonoBehaviour
    {
        public Wheel RearWheel => rearWheel;
        public Wheel FrontWheel => frontWheel; 
        
        [SerializeField]
        private Wheel rearWheel;

        [SerializeField]
        private Wheel frontWheel;

        [SerializeField]
        private Rigidbody2D bodyRigidbody;

        [SerializeField]
        private float rearWheelPower;

        [SerializeField]
        private float fullWheelSpeed;

        [SerializeField, Range(0, 1)]
        private float backMovementSpeedPowerRatio = 0.5f;

        [SerializeField]
        private float tiltForce = 1;

        public void Accelerate(float deltaTime)
        {
            rearWheel.Rigidbody2D.angularVelocity = Mathf.Min(rearWheel.Rigidbody2D.angularVelocity, 0);
            frontWheel.Rigidbody2D.angularVelocity = Mathf.Min(frontWheel.Rigidbody2D.angularVelocity, 0);
            
            Move(1, rearWheelPower, fullWheelSpeed, deltaTime);
            Tilt(1, tiltForce, deltaTime);
        }

        public void Brake(float deltaTime)
        {
            rearWheel.Rigidbody2D.angularVelocity = Mathf.Max(rearWheel.Rigidbody2D.angularVelocity, 0);
            frontWheel.Rigidbody2D.angularVelocity = Mathf.Max(frontWheel.Rigidbody2D.angularVelocity, 0);
            
            float rear = Mathf.Lerp(0, rearWheelPower, backMovementSpeedPowerRatio);
            float full = Mathf.Lerp(0, fullWheelSpeed, backMovementSpeedPowerRatio);
            Move(-1, rear, full, deltaTime);
            Tilt(-1, tiltForce, deltaTime);
        }

        public void Jump(float deltaTime)
        {
            bodyRigidbody.AddForce(Vector2.up * 10, ForceMode2D.Impulse);
        }

        private void Move(int direction, float rearPower, float fullSpeed, float deltaTime)
        {
            Vector3 forceDirection = bodyRigidbody.transform.rotation * (direction * Vector2.right);

            if (rearWheel.IsOnGround())
            {
                bodyRigidbody.AddForce(deltaTime * fullSpeed * forceDirection, ForceMode2D.Force);
            }

            rearWheel.Rigidbody2D.AddTorque(direction * -rearPower * deltaTime, ForceMode2D.Force);
        }

        private void Tilt(int direction, float force, float deltaTime)
        {
            Vector3 forceDirection = bodyRigidbody.transform.rotation * (direction * Vector2.up);
            frontWheel.Rigidbody2D.AddForce(forceDirection * force * deltaTime, ForceMode2D.Force);
        }
    }
}