using UnityEngine;

namespace Bike
{
    public class Wheel : MonoBehaviour
    {
        public Rigidbody2D Rigidbody2D => this.rb2d;

        public Joint2D Joint2D => this.joint;

        [SerializeField]
        private Joint2D joint;

        [SerializeField]
        private Rigidbody2D rb2d;

        [SerializeField]
        private SpriteRenderer wheelSprite;

        [SerializeField]
        private ContactFilter2D contactFilter;


        public bool IsOnGround()
        {
            return rb2d.IsTouching(contactFilter);
        }
    }
}