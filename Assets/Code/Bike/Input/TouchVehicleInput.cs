using UnityEngine;

namespace Bike.Input
{
    public class TouchVehicleInput : IVehicleInput
    {
        public bool Accelerate { get; private set; }
        public bool Brake { get; private set; }
        public bool Jump { get; private set; }

        public void Tick()
        {
            for (var touchIndex = 0; touchIndex < UnityEngine.Input.touchCount; touchIndex++)
            {
                Touch touch = UnityEngine.Input.GetTouch(touchIndex);

                if (touch.position.x < Screen.width / 2f)
                {
                    Brake = true;
                }
                else
                {
                    Accelerate = true;
                }
            }
        }

        public void Reset()
        {
            Brake = Accelerate = Jump = false;
        }
    }
}