using UnityEngine;

namespace Bike.Input
{
    public class KeyboardVehicleInput : IVehicleInput
    {
        public bool Accelerate { get; private set; }
        public bool Brake { get; private set; }
        public bool Jump { get; private set; }

        public void Tick()
        {
            if (UnityEngine.Input.GetKey(KeyCode.D))
            {
                Accelerate = true;
            }

            if (UnityEngine.Input.GetKey(KeyCode.A))
            {
                Brake = true;
            }

            if (UnityEngine.Input.GetKeyDown(KeyCode.Space))
            {
                Jump = true;
            }
        }

        public void Reset()
        {
            Accelerate = Brake = Jump = false;
        }
    }
}