namespace Bike.Input
{
    interface IVehicleInput
    {
        bool Accelerate { get; }
        bool Brake { get; }
        bool Jump { get; }

        void Tick();
        void Reset();
    }
}