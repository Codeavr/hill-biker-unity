using System;
using System.Collections.Generic;
using System.Linq;
using MeshGeneration;
using UnityEngine;

namespace Terrain2D
{
    [RequireComponent(typeof(MeshFilter))]
    public class Chunk : MonoBehaviour
    {
        private MeshFilter[] _meshFilters;
        private PolygonCollider2D _collider;

        private void Awake()
        {
            _meshFilters = GetComponentsInChildren<MeshFilter>();
            _collider = GetComponent<PolygonCollider2D>();
        }

        public void GenerateFromPath(IReadOnlyList<Vector2> path, float minHeight, float width)
        {
            Mesh mesh = _meshFilters.First().mesh;
            ChunkMeshGenerator.Generate(new ChunkMeshGenerationData(path, minHeight, width), mesh);

            Array.ForEach(_meshFilters, meshFilter => meshFilter.sharedMesh = mesh);

            _collider.points = path.ToArray();
        }
    }
}