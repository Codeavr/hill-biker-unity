using Terrain2D.Generation;
using UnityEngine;

namespace Terrain2D
{
    [CreateAssetMenu(fileName = "TerrainData", menuName = "Objects/Terrain2D", order = 0)]
    public class ProceduralTerrainData : ScriptableObject
    {
        public float Width => width;

        public float MinStep => minStep;

        public float Accuracy => accuracy;

        public float ChunkMinHeight => chunkMinHeight;

        public float YFactor => yFactor;

        public PerlinLayers PerlinLayers => perlinLayers;

        public Chunk TerrainChunkPrototype => terrainChunkPrototype;

        [SerializeField]
        private float width = 10;

        [SerializeField]
        private float minStep = 0.1f;

        [SerializeField]
        [Range(0.0001f, 0.9999f)]
        private float accuracy = 0.1f;

        [SerializeField]
        private float chunkMinHeight = 5;
        
        [SerializeField]
        private float yFactor = 15f;

        [SerializeField]
        private PerlinLayers perlinLayers;

        [SerializeField]
        private Chunk terrainChunkPrototype;
    }
}