using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Terrain2D.Generation
{
    public static class PathGenerator
    {
        public static List<Vector2> Generate(Func<float, float> pathFunction,
                                               float offset,
                                               float accuracy,
                                               float width,
                                               float minHeight,
                                               float minStep)
        {
            var path = new List<Vector2>();
            Generate(pathFunction, offset, accuracy, width, minHeight, minStep, path);

            return path;
        }

        public static void Generate(Func<float, float> pathFunction,
                                      float offset,
                                      float accuracy,
                                      float width,
                                      float minHeight,
                                      float minStep,
                                      List<Vector2> cache)
        {
            float PathFunction(float x)
            {
                return pathFunction(x + offset);
            }
            
            accuracy = Math.Min(minStep, accuracy);

            List<Vector2> path = cache;
            path.Clear();

            path.Add(new Vector2(0, PathFunction(0)));
            float lowest = 0;
            
            for (float x = 0; x < width; x = Mathf.Clamp(x + accuracy, 0, width))
            {
                float y = PathFunction(x);
                lowest = Mathf.Min(lowest, y);
                
                var currentPoint = new Vector2(x, y);

                if (Vector2.Distance(path.Last(), currentPoint) >= minStep)
                {
                    path.Add(currentPoint);
                }
            }

            path.Insert(0, new Vector2(0, lowest - minHeight));
            path.Add(new Vector2(width, PathFunction(width)));
            path.Add(new Vector2(width, lowest - minHeight));
        }
        
    }
}