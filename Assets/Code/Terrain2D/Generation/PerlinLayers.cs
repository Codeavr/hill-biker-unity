using System;
using System.Linq;
using UnityEngine;

namespace Terrain2D.Generation
{
    [Serializable]
    [CreateAssetMenu(fileName = "PerlinLayer", menuName = "Objects/Perlin Layer", order = 0)]
    public class PerlinLayers : ScriptableObject
    {
        [SerializeField]
        private Perlin2DLayer[] layers;

        private float _seed;

        public void SetSeed(float seed)
        {
            _seed = seed;
        }

        public float Evaluate(float value)
        {
            float powersSum = layers.Sum(l => l.Power);
            float layerValues = layers.Sum(l => l.Evaluate(value, _seed) * (l.Power / powersSum));

            return layerValues;
        }

        #if UNITY_EDITOR
        private void OnValidate()
        {
            Editor_FlattenPowers();
        }

        private void Editor_FlattenPowers()
        {
            float powersSum = layers.Sum(l => l.Power);

            if (Mathf.Approximately(powersSum, 0f) || float.IsNaN(powersSum) || float.IsInfinity(powersSum))
            {
                return;
            }
            
            
            foreach (Perlin2DLayer layer in layers)
            {
                layer.Power /= powersSum;
            }
        }
        #endif
    }
}