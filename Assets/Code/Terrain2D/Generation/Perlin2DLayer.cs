using System;
using UnityEngine;

namespace Terrain2D.Generation
{
    [Serializable]
    public class Perlin2DLayer
    {
        public float Power
        {
            get => power;
            set => power = value;
        }

        [SerializeField, Range(0f, 1f)]
        private float power = 1f;

        [SerializeField]
        private float scale = 1;

        [SerializeField]
        private float offset;

        [SerializeField]
        private string comment;

        public float Evaluate(float value, float seed)
        {
            return Remap(Mathf.PerlinNoise(seed, (offset + value) * scale));
        }

        private static float Remap(float value)
        {
            return value * 2 - 1f;
        }
    }
}