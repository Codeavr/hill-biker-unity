﻿using System;
using System.Collections.Generic;
using Terrain2D.Generation;
using UnityEngine;

namespace Terrain2D
{
    public class ProceduralTerrain : MonoBehaviour
    {
        public Transform Target
        {
            get => watchable;
            set => watchable = value;
        }

        [SerializeField]
        private Transform watchable;

        [SerializeField]
        private ProceduralTerrainData terrainData;

        [SerializeField]
        private bool useTimeAsSeed;

        [SerializeField]
        private int seed;

        [SerializeField]
        private int maxChunksActive = 5;

        [SerializeField]
        private int chunksShift = 1;
        
        private List<Chunk> _loadedChunks = new List<Chunk>();
        private Queue<Chunk> _chunksToRemoveQueue = new Queue<Chunk>();

        private void Awake()
        {
            if (useTimeAsSeed)
            {
                seed = DateTime.Now.GetHashCode();
            }
            terrainData.PerlinLayers.SetSeed(seed);
        }

        private void Update()
        {
            float watchableX = watchable != null ? ToGridPosition(watchable.position.x) : ToGridPosition(0);

            DestroyChunksOutOfBounds(watchableX);
            LoadChunksInBounds(watchableX);
        }

        private void LoadChunksInBounds(float watchableX)
        {
            for (int i = -maxChunksActive / 2; i < maxChunksActive / 2; i++)
            {
                float x = i * terrainData.Width + watchableX;
                Chunk chunkOnPosition = _loadedChunks.Find(chunk => Mathf.Approximately(chunk.transform.position.x, x));

                if (!chunkOnPosition)
                {
                    _loadedChunks.Add(CreateChunk(x));
                }
            }
        }

        private void DestroyChunksOutOfBounds(float watchableX)
        {
            float leftBound = -maxChunksActive / 2 * terrainData.Width + watchableX;
            float rightBound = maxChunksActive / 2 * terrainData.Width + watchableX;

            for (var index = 0; index < _loadedChunks.Count; index++)
            {
                Chunk chunk = _loadedChunks[index];

                if (chunk.transform.position.x < leftBound || chunk.transform.position.x > rightBound)
                {
                    _chunksToRemoveQueue.Enqueue(chunk);
                }
            }

            while (_chunksToRemoveQueue.Count > 0)
            {
                Chunk chunk = _chunksToRemoveQueue.Dequeue();
                _loadedChunks.Remove(chunk);
                Destroy(chunk.gameObject);
            }
        }

        private float ToGridPosition(float rawX)
        {
            return (Mathf.Floor(rawX / terrainData.Width) + chunksShift) * terrainData.Width;
        }

        private Chunk CreateChunk(float x)
        {
            var position = new Vector3(x, terrainData.YFactor / 2f);
            
            List<Vector2> path = PathGenerator.Generate(GenerationFunction,
                                                        position.x,
                                                        terrainData.Accuracy,
                                                        terrainData.Width,
                                                        terrainData.ChunkMinHeight,
                                                        terrainData.MinStep);

            Chunk chunk = Instantiate(terrainData.TerrainChunkPrototype, position, Quaternion.identity, transform);
            chunk.GenerateFromPath(path, terrainData.ChunkMinHeight, terrainData.Width);

            return chunk;
        }

        private float GenerationFunction(float x)
        {
            float perlin = terrainData.PerlinLayers.Evaluate(x);

            return perlin * terrainData.YFactor;
        }
    }
}