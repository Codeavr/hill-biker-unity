﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    [SerializeField]
    private int targetFramerate = 30;

    private void Start()
    {
        Application.targetFrameRate = targetFramerate;
    }
}
